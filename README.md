# Pricing

## Test technique pour Sellermania

### Installation

> Attention : Il n'est pas nécessaire de réaliser ce qui suit si vous disposez déjà d'un environnement PHP prêt à l'usage.

Pour faciliter l'installation d'environnement, j'ai préféré utiliser  [Xampp](https://downloadsapachefriends.global.ssl.fastly.net/7.4.21/xampp-windows-x64-7.4.21-1-VC15-installer.exe?from_af=true).

Cette version propose la configuration suivante :

* PHP 7.4.21;
* Apache 2.0; 
* MariaDB 10.4.20.

MariaDB sera remplacé par MySql 5.7 si le chrono le permet en suivant les instructions [ici](https://ourcodeworld.com/articles/read/1215/how-to-use-mysql-5-7-instead-of-mariadb-in-xampp-for-windows).

Pour installer Composer, il suffit de suivre les instructions de [cette page](https://thecodedeveloper.com/install-composer-windows-xampp/).

Normalement, l'environnement devrait être prêt.
